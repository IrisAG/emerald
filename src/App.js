import React, { Component } from "react";
import Navbar from "./components/Navbar";
import NewReport from "./components/Navbar";
import loginContainer from "./containers/general/logingContainer";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />

          {/* General */}
          {/* <Router exact path="/" component={} /> */}
          {/* Admin */}
          {/* Supervisor */}
          {/* Capturista */}
        </div>
      </Router>
    );
  }
}

export default App;
