import { combineReducers } from "redux";

import { reducer as reduxForm } from "redux-form";
// import { createSelector } from "reselect";
import { locality } from "./locality";

export default combineReducers({
  form: reduxForm
});

// export const getLocality = createSelector(
//   state => state.locality,
//   locality => locality
// );
