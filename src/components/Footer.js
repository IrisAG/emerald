import React from "react";
import { Row, Col } from "reactstrap";
import "./styles.css";

export default function Footer() {
  return (
    <div className="container-fluid">
      <Row>
        <Col className="footer">
          <p className="footer-title">
            Emerald Bioagriculture Powered by Unima © COPYRIGHT 2020
          </p>
        </Col>
      </Row>
    </div>
  );
}
