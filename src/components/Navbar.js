import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import { Nav, NavItem, NavLink } from "reactstrap";
import "./styles.css";

import Dashboard from "./general/dashboardComponent/dashboardComponent";
// import Notification from "./general/dashboardComponent/notifications";
import Users from "./general/usersComponents/usersComponent";
import NewRegister from "./general/newRegisterComponent/newRegisterComponent";
import Register from "./general/registersComponent/registerComponent";
import NewReport from "./general/newReportComponent/newReportComponent";
import Footer from "./Footer";

const drawerWidth = 110;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(6)
  }
}));

const LightTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: "#615E5E",
    color: "#FFFFFF",
    boxShadow: theme.shadows[1],
    fontSize: 13,
    fontFamily: "Raleway",
    fontWeight: "500",
    lineHeight: "15px",
    fontStyle: "normal"
  }
}))(Tooltip);

export default function Navbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer className={classes.drawer} variant="permanent" anchor="left">
        <Nav vertical className="container-fluid">
          <NavItem>
            <img src="../images/logo.png" alt=" " className="navbarLogo" />
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <LightTooltip title="Dashboard" placement="right">
                <img src="../images/Updates.png" alt=" " />
              </LightTooltip>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <LightTooltip title="Usuarios" placement="right">
                <img src="../images/USUARIOS.png" alt=" " />
              </LightTooltip>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <LightTooltip title="Registros" placement="right">
                <img src="../images/registros.png" alt=" " />
              </LightTooltip>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <LightTooltip title="Nuevo registro" placement="right">
                <img
                  src="../images/NUEVO REGISTRO.png"
                  alt=" "
                  className="img-link"
                />
              </LightTooltip>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <LightTooltip title="Nuevo reporte" placement="right">
                <img src="../images/REPORTES.png" alt=" " />
              </LightTooltip>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">
              <img src="../images/Salir.png" alt=" " />
            </NavLink>
          </NavItem>
        </Nav>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Typography>
          {/* <Dashboard /> */}
          {/* <Notification /> */}
          {/* <Users /> */}
          {/* <NewRegister /> */}
          {/* <Register /> */}
          <NewReport />
        </Typography>
        <Footer />
      </main>
    </div>
  );
}
