import React from "react";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText
} from "@material-ui/core";

export const renderTextField = props => {
  const {
    input,
    id,
    type,
    label,
    meta: { error, visited }
  } = props;
  return (
    <FormControl
      style={{ width: "400px" }}
      {...input}
      error={error !== undefined && visited === true ? true : false}
    >
      <InputLabel htmlFor="adornment-password">{label}</InputLabel>
      <Input id={id} type={type} value={input.value} component="input" />
      <FormHelperText id="component-error-text">
        {error !== undefined && visited === true ? error : ""}
      </FormHelperText>
    </FormControl>
  );
};
