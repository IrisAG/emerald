import React from "react";
import { Container, Col, Row } from "reactstrap";
import { Field, reduxForm } from "redux-form";
import { renderTextField } from "../Form";
import { Button } from "@material-ui/core";
import useStyles from "../../../style";

const LoginComponent = props => {
  const { handleSubmit, initialvalues } = props;
  const classes = useStyles();
  return (
    <div>
      <Container fluid={true}>
        <Row className={classes.backgroundBody}>
          <Col xs={{ size: 3, offset: 8 }} className={classes.form}>
            <Container>
              <Row>
                <Col>
                  <img alt="logo" src="./../../images/logo-emerald_1.png" />
                </Col>

                <Col>
                  <br />
                  <h3 className={classes.subtitleGreenDark}>
                    PORTAL DE REPORTES
                  </h3>
                </Col>

                <Col>
                  <br />
                  <br />
                  <h4 className={classes.subtitleVerdeLight}>Iniciar sesión</h4>
                  <form onSubmit={handleSubmit} initialvalues={initialvalues}>
                    <Field
                      id="email"
                      name="email"
                      label="Correo electrónico"
                      type="text"
                      component={renderTextField}
                    />
                    <br />
                    <br />
                    <Field
                      id="password"
                      name="password"
                      label="Contraseña"
                      type="password"
                      component={renderTextField}
                    />
                    <br />
                    <br />
                    <br />
                    <Button className={classes.buttonMain} type="submit">
                      Entrar
                    </Button>
                  </form>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>
      <div className={classes.footer}>
        <p className={classes.footerParagraph}>
          Emerald Bioagriculture Powered by Unima © COPYRIGHT 2020
        </p>
      </div>
    </div>
  );
};

export default reduxForm({
  form: "LoginComponent",
  enableReinitialize: true
})(LoginComponent);
