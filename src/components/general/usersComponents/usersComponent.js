import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Moment from "react-moment";
import "moment-timezone";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input,
  FormGroup,
  Card,
  CardBody
} from "reactstrap";
import { Divider, ExpansionPanelActions } from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import "../../styles.css";

const date = new Date();

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },
  heading: {
    fontSize: "18px",
    fontWeight: theme.typography.fontWeightNormal,
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    lineHeight: "23px",
    color: "#1B1F01"
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
    boxShadow: "none",
    alignItems: "center"
  },
  textField: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#615E5E"
  },
  options: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "20px",
    textAlign: "justify"
  },
  input: {
    border: "none",
    borderBottom: "1px solid #424F3E",
    width: "300px",
    background: "#FDFDFD"
  },
  buttonCollapse: {
    margin: theme.spacing(1),
    background: "#6b8e23",
    border: "5px solid #6B8E23",
    color: " #FFFFFF"
  },
  cardContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  tableContainer: {
    marginTop: "50px",
    background: "#F8F7F7"
  },
  tableHead: {
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "18px",
    lineHeight: "21px",
    alignItems: "center",
    letterSpacing: "-0.015em",
    color: "#1C2000",
    textAlign: "justify"
  },
  table: {
    minWidth: 650
  }
}));

/**
 *
 * @param {ExpansionPanel, table } props
 * -ExpansionPanel.-Arraylist
 *  - title.- String -> Nombre del panel de expansion
 *  - subtitle.- String -> Nombre de los campos
 *  - input.- String -> Nombre del usuario
 *  - input.- String -> Correo electrónico
 *  - input-select.- String -> Rol del usuario
 *  - boton .- -> Crear usuarios
 * -Table.- ArrayList
 *  - title.- String -> Nombre de cada una de las columnas
 *  - subtitles.- String -> Nombres de los usuarios activos y roles
 *  - date.- fecha de alta de cada usuario
 *  - icono-boton editar
 *  - icono-boton eliminar
 */

const Users = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  function createData(name, userRole, entryDay, editUser, deleteUser) {
    return { name, userRole, entryDay, editUser, deleteUser };
  }

  const rows = [
    createData(
      "Arturo Cervantes",
      "Administrador",
      "02-01-2020",
      <EditIcon />,
      <DeleteForeverIcon />
    ),
    createData(
      "Francisco Medina",
      "Supervisor",
      "02-01-2020",
      <EditIcon />,
      <DeleteForeverIcon />
    ),
    createData(
      "Sara Gómez",
      "Reportes",
      "02-01-2020",
      <EditIcon />,
      <DeleteForeverIcon />
    )
  ];

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <p className="principal-title">Administración de usuarios</p>
          <p className="secundary-title">Asignación, alta y baja de usuarios</p>
        </Grid>
        <Grid item xs={6}>
          <Moment format={"dddd DD MMMM YYYY"} className="time">
            {date}
          </Moment>

          <Button onClick={toggle} className={classes.notificationButton}>
            <NotificationsIcon color="#dadada" />5 {buttonLabel}
          </Button>

          <Modal
            isOpen={modal}
            toggle={toggle}
            className={className}
            style={{
              marginTop: 0,
              width: "380px",
              right: "0px",
              position: "fixed"
            }}
          >
            <ModalHeader toggle={toggle}>Panel de notificaciones</ModalHeader>
            <ModalBody style={{ height: "100%" }}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{
                        fontWeight: "bold",
                        marginBottom: "3px",
                        marginTop: "10px"
                      }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "20px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider />
            </ModalBody>
          </Modal>
        </Grid>
        <Grid item xs={12}>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                Crear nuevo usuario
              </Typography>
            </ExpansionPanelSummary>

            <ExpansionPanelDetails>
              <Grid item xs>
                <Paper className={classes.paper}>
                  <Label for="standard-basic" className={classes.textField}>
                    Nombre de usuario
                  </Label>
                  <Input
                    type="text"
                    name="userName"
                    id="standard-basic"
                    className={classes.input}
                  />
                </Paper>
              </Grid>
              <Grid item xs>
                <Paper className={classes.paper}>
                  <Label for="standard-basic" className={classes.textField}>
                    Correo electrónico
                  </Label>
                  <Input
                    type="email"
                    name="email"
                    id="standard-basic"
                    className={classes.input}
                  />
                </Paper>
              </Grid>
              <Grid item xs>
                <Paper className={classes.paper}>
                  <FormGroup>
                    <Label for="select-item" className={classes.textField}>
                      Rol de usuario
                    </Label>
                    <Input
                      type="select"
                      name="select"
                      id="select-item"
                      className={classes.input}
                      style={{ width: "250px" }}
                    >
                      <option className={classes.options} color="#615E5E">
                        seleccionar
                      </option>
                    </Input>
                  </FormGroup>
                </Paper>
              </Grid>
            </ExpansionPanelDetails>
            <ExpansionPanelActions>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Button variant="contained" className={classes.buttonCollapse}>
                  <PersonAddIcon /> Crear usuario
                </Button>
              </Grid>
            </ExpansionPanelActions>
          </ExpansionPanel>
        </Grid>
      </Grid>
      <Card className={classes.cardContainer}>
        <CardBody>
          <Grid item xs={12}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.tableHead}>
                      Usuarios activos
                    </TableCell>
                    <TableCell className={classes.tableHead}>
                      Rol de usuario
                    </TableCell>
                    <TableCell className={classes.tableHead}>
                      Fecha de alta
                    </TableCell>
                    <TableCell className={classes.tableHead}>Editar</TableCell>
                    <TableCell className={classes.tableHead}>
                      Eliminar
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map(row => (
                    <TableRow key={row.name}>
                      <TableCell
                        component="th"
                        scope="row"
                        color=" #333333"
                        letterSpacing="-0.15em"
                        className={classes.options}
                      >
                        {row.name}
                      </TableCell>
                      <TableCell
                        align="right"
                        color=" #333333"
                        letterSpacing="-0.15em"
                        className={classes.options}
                      >
                        {row.userRole}
                      </TableCell>
                      <TableCell
                        align="right"
                        color=" #333333"
                        letterSpacing="-0.15em"
                        className={classes.options}
                      >
                        {row.entryDay}
                      </TableCell>
                      <TableCell
                        align="right"
                        color=" #333333"
                        className={classes.options}
                      >
                        {row.editUser}
                      </TableCell>
                      <TableCell
                        align="right"
                        color=" #333333"
                        className={classes.options}
                      >
                        {row.deleteUser}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </CardBody>
      </Card>
    </div>
  );
};
export default Users;
