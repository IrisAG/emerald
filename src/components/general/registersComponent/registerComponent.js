import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import NotificationsIcon from "@material-ui/icons/Notifications";
import FormGroup from "@material-ui/core/FormGroup";
import DateFnsUtils from "@date-io/date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import SearchIcon from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import FilterListIcon from "@material-ui/icons/FilterList";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import "date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Moment from "react-moment";
import "moment-timezone";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input,
  Card,
  CardBody
} from "reactstrap";
import {
  Divider,
  ExpansionPanelActions,
  TextField,
  FormControl,
  Select
} from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import "../../styles.css";

/**
 *
 *  @param {} props
 */

const date = new Date();

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },

  heading: {
    fontSize: "18px",
    fontWeight: theme.typography.fontWeightNormal,
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    lineHeight: "23px",
    color: "#1B1F01"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "justify",
    color: theme.palette.text.secondary,
    boxShadow: "none",
    alignItems: "center"
  },
  paperRegister: {
    padding: theme.spacing(2),
    textAlign: "justify",
    boxShadow: "none",
    alignItems: "center",
    background: "#fafafa"
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: "330px",
    textAlign: "justify",
    border: "1px solid #5d6e02"
  },
  textField: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#615E5E"
  },
  options: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px"
  },
  input: {
    borderBottom: "1px solid #5d6e02",
    background: "#FDFDFD",
    textAlign: "left"
  },
  inputDate: {
    position: "relative",
    top: "-13px",
    borderBottom: "1px solid #424F3E"
  },
  addButton: {
    margin: theme.spacing(2),
    background: "#6b8e23",
    border: "5px solid #6B8E23",
    color: " #FFFFFF",
    borderRadius: "50px",
    width: "35px",
    height: "35px",
    left: "-30px",
    position: "relative"
  },
  cardContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  tableContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  }
}));

const Register = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const handleDateChange = date => {
    setSelectedDate(date);
  };

  function createData(
    status,
    locality,
    country,
    state,
    town,
    crop,
    producer,
    sowingSeason
  ) {
    return {
      status,
      locality,
      country,
      state,
      town,
      crop,
      producer,
      sowingSeason
    };
  }

  const rows = [
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    ),
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    ),
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    ),
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    ),
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    ),
    createData(
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Xonotli S.A de C.V.",
      "Primavera"
    )
  ];

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={7}>
          <p className="principal-title">Registros</p>
          <p className="secundary-title">Listado de registros generados</p>
          <p className="subtitle">
            Seleccionar parámetros de filtrado para ver la información deseada
          </p>
        </Grid>

        <Grid item xs={5}>
          <Moment format={"dddd DD MMMM YYYY"} className="time">
            {date}
          </Moment>

          <Button onClick={toggle} className={classes.notificationButton}>
            <NotificationsIcon color="#dadada" />5 {buttonLabel}
          </Button>

          <Modal
            isOpen={modal}
            toggle={toggle}
            className={className}
            style={{
              marginTop: 0,
              width: "380px",
              right: "0px",
              position: "fixed"
            }}
          >
            <ModalHeader toggle={toggle}>Panel de notificaciones</ModalHeader>
            <ModalBody style={{ height: "100%" }}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{
                        fontWeight: "bold",
                        marginBottom: "3px",
                        marginTop: "10px"
                      }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "20px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider />
            </ModalBody>
          </Modal>
        </Grid>
        <Grid container spacing={3} direction="row" justify="flex-end">
          <Grid item xs={3}>
            <Paper className={classes.paper}>
              <TextField
                className={classes.input}
                id="register-search"
                label=""
                defaultValue="Buscar"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  )
                }}
              />
            </Paper>
          </Grid>
        </Grid>

        <Grid container xs spacing={3}>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-locality-label"
                style={{ marginLeft: "20px" }}
              >
                Localidad
              </InputLabel>
              <Select
                labelId="register-locality-label"
                id="register-locality"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Localidad uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-country-label"
                style={{ marginLeft: "20px" }}
              >
                País
              </InputLabel>
              <Select
                labelId="register-country-label"
                id="register-country"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-state-label"
                style={{ marginLeft: "20px" }}
              >
                Estado
              </InputLabel>
              <Select
                labelId="register-state-label"
                id="register-state"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-town-label"
                style={{ marginLeft: "20px" }}
              >
                Municipio
              </InputLabel>
              <Select
                labelId="register-town-label"
                id="register-town"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>

          {/* segunda linea */}

          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-crop-label"
                style={{ marginLeft: "20px" }}
              >
                Cultivo
              </InputLabel>
              <Select
                labelId="register-crop-label"
                id="register-crop"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}> uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-producer-label"
                style={{ marginLeft: "20px" }}
              >
                Productor
              </InputLabel>
              <Select
                labelId="register-producer-label"
                id="register-producer"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-sowingSeason-label"
                style={{ marginLeft: "20px" }}
              >
                Época de siembra
              </InputLabel>
              <Select
                labelId="register-sowingSeason-label"
                id="register-sowingSeason"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-treatment-label"
                style={{ marginLeft: "20px" }}
              >
                Tratamientos
              </InputLabel>
              <Select
                labelId="register-treatment-label"
                id="register-treatment"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>

          {/* tercer linea */}
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="register-ingredients-label"
                style={{ marginLeft: "20px" }}
              >
                Ingrediente
              </InputLabel>
              <Select
                labelId="register-ingredients-label"
                id="register-ingredients"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid
            item
            xs
            style={{ textAlign: "left", margin: "0px 0px 0px 15px" }}
          >
            <p className="subtitle-date">Fechas de plantación</p>
            <Grid container direction="row" spacing={3}>
              <Grid item xs={4}>
                <Paper className={classes.paperRegister}>
                  <FormGroup>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="register-plantingDate"
                        label="Desde esta fecha"
                        value={selectedDate}
                        onChange={handleDateChange}
                        className={classes.inputDate}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormGroup>
                </Paper>
              </Grid>
              <p style={{ margin: "50px 10px 0px 10px" }}> - </p>
              <Grid item xs={4}>
                <Paper className={classes.paperRegister}>
                  <FormGroup>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="register-plantingDate"
                        label="Hasta esta fecha"
                        value={selectedDate}
                        onChange={handleDateChange}
                        className={classes.inputDate}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormGroup>
                </Paper>
              </Grid>
              <Grid item xs>
                <FormGroup>
                  <Button style={{ background: "#6b8e23" }}>
                    <FilterListIcon /> FILTRAR INFORMACIÓN
                  </Button>
                </FormGroup>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* tabla */}
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.cardContainer}>
              <CardBody>
                <Grid item xs={12}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell className={classes.tableHead}>
                            Status
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Localidad <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            País <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Estado <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Municipio <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Cultivo <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Productor <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Época de siembra <ImportExportIcon />
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {rows.map(row => (
                          <TableRow key={row.locality}>
                            <TableCell>
                              <Button style={{ background: "#6b8e23" }}>
                                Aprobar
                              </Button>
                            </TableCell>
                            <TableCell
                              component="th"
                              scope="row"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.locality}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.country}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.state}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.town}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.crop}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.producer}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.sowingSeason}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </CardBody>
            </Card>
          </Grid>
        </Grid>
      </Grid>{" "}
      {/* contenedor */}
    </div>
  );
};
export default Register;
