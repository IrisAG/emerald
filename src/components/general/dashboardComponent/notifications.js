import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input
} from "reactstrap";
import PublicIcon from "@material-ui/icons/Public";
import Grid from "@material-ui/core/Grid";
import { Divider } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Moment from "react-moment";
import "moment-timezone";
import "../../styles.css";
import { differenceInCalendarISOWeeks } from "date-fns";

const date = new Date();

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  button: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "40px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },
  input: {
    border: "none",
    borderBottom: "1px solid #424F3E",
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "16px",
    lineHeight: "22px",
    color: "#424F3E",
    textAlign: "left"
  },
  title: {
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "18px",
    lineHeight: "21px",
    color: "#000000",
    marginTop: "20px",
    textAlign: "center"
  },
  subtitle: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "20px",
    color: "#000000",
    marginTop: "20px",
    textAlign: "center",
    width: "400px"
  },
  titleField: {
    fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "12px",
    lineHeight: "26px",
    color: "#615E5E",
    marginTop: "20px",
    position: "relative"
  },
  modalButtom: {
    background: "#6B8E23",
    border: "5px solid #6B8E23",
    margin: "8px",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "16px",
    lineHeight: "20px",
    color: "#FFFFFF",
    textAlign: "center",
    marginTop: "35px",
    alignContent: "center",
    marginRight: "55px"
  }
}));

const NotificationButton = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button onClick={toggle} className={classes.button}>
        {buttonLabel}
      </Button>
      <Grid item xs>
        <Modal isOpen={modal} toggle={toggle} className={className}>
          <ModalHeader toggle={toggle}>
            <Grid
              item
              xs
              direction="column"
              justify="center"
              alignItems="center"
            >
              <p className={classes.title}>
                Verificación de campos y traducciones
              </p>
              <p className={classes.subtitle}>
                Se ha solicitado la verificación de la siguientes traducciones
                para el campo de LOCALIDAD
              </p>
            </Grid>
          </ModalHeader>

          <ModalBody style={{ margin: "auto" }}>
            <Grid container xs={10} direction="column" justify="center">
              <Label for="localidad1" className={classes.titleField}>
                Español
              </Label>
              <Input
                type="text"
                placeholder="Localidad 1"
                className={classes.input}
              />

              <Label for="localidad1" className={classes.titleField}>
                Inglés
              </Label>
              <Input
                type="text"
                placeholder="Localidad 1"
                className={classes.input}
              />

              <Label for="localidad1" className={classes.titleField}>
                Portugués
              </Label>
              <Input
                type="text"
                placeholder="Localidad 1"
                className={classes.input}
              />
            </Grid>
            <Grid item>
              <Button className={classes.modalButtom}>GUARDAR DATOS</Button>
              <Button className={classes.modalButtom}>CANCELAR</Button>
            </Grid>
          </ModalBody>
        </Modal>
      </Grid>
    </div>
  );
};

export default NotificationButton;
