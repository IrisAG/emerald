import React, { useState } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Moment from "react-moment";
import "moment-timezone";
import "../../styles.css";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { Divider } from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";

const date = new Date();
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(3),
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "15px",
    lineHeight: "18px",
    color: "#ffffff",
    position: "relative",
    background: "#424f3e",
    border: "7px solid #424f3e",
    margin: "auto",
    maxWidth: "450px",
    minWidth: "450px"
  },
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },
  ellipse: {
    height: 150,
    width: 150,
    borderRadius: "50%",
    border: "5px solid #8fa685",
    background: "#5b6c55",
    marginLeft: 20,
    fontFamily: "Poppins",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "38px",
    lineHeight: "57px",
    alignItems: "center",
    textAlign: "center",
    color: "#e8f1e0"
  },
  middleLine: {
    borderLeft: "3px solid white",
    height: 120,
    width: 0.09,
    left: "-25px",
    marginTop: "20px",
    position: "relative"
  },
  cardTitle: {
    left: 0,
    position: "relative",
    top: "20px",
    textAlign: "left",
    alignItems: "center",
    letterSpacing: "-0.015em",
    width: "137px"
  },
  cardSubtitle: {
    left: 0,
    position: "relative",
    top: "40px",
    textAlign: "left",
    color: "#85be71",
    padding: "0 10px 0",
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "15px",
    letterSpacing: "-0.015em",
    alignItems: "center"
  },
  cardSubtitle2: {
    left: 0,
    position: "relative",
    top: "40px",
    textAlign: "left",
    color: "#ffffff",
    padding: "0 10px 0",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: "-0.015em",
    alignItems: "center"
  },
  gridList: {
    marginTop: "50px",
    background: "#ffffff",
    boxShadow: "0px 4px 4px rgba(0,0,0,.25)",
    marginBottom: "1em"
  },
  listTitle: {
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "18px",
    lineHeight: "21px",
    letterSpacing: "-0.015em",
    alignItems: "center",
    color: "#000000",
    textAlign: "left"
  },
  listSubtitle: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "16px",
    lineHeight: "20px",
    textAlign: "justify",
    letterSpacing: "-0.015em",
    color: "#000000",
    verticalAlign: "top"
  }
}));

const Dashboard = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <p className="principal-title">Tablero de actualizaciones</p>
          <p className="secundary-title">Vista general de actualizaciones</p>
        </Grid>
        <Grid item xs={6}>
          <Moment format={"dddd DD MMMM YYYY"} className="time">
            {date}
          </Moment>

          <Button onClick={toggle} className={classes.notificationButton}>
            <NotificationsIcon color="#dadada" />5 {buttonLabel}
          </Button>

          <Modal
            isOpen={modal}
            toggle={toggle}
            className={className}
            style={{
              marginTop: 0,
              width: "380px",
              right: "0px",
              position: "fixed"
            }}
          >
            <ModalHeader toggle={toggle}>Panel de notificaciones</ModalHeader>
            <ModalBody style={{ height: "100%" }}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{
                        fontWeight: "bold",
                        marginBottom: "3px",
                        marginTop: "10px"
                      }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "20px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider />
            </ModalBody>
          </Modal>
        </Grid>
        <Paper className={classes.paper}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item>
                <p className={classes.cardTitle}>
                  Registros pendientes para aprobación
                </p>
                <p className={classes.cardSubtitle}>APROBAR REGISTROS</p>
              </Grid>
            </Grid>
            <Grid item>
              <div className={classes.middleLine}></div>
            </Grid>
            <Grid item>
              <button
                className={classes.ellipse}
                style={{ background: "#424F3E" }}
              >
                25
              </button>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item>
                <p className={classes.cardTitle}>Registros aprobados</p>
                <p className={classes.cardSubtitle2}>hasta 21.01.2020</p>
              </Grid>
            </Grid>
            <Grid item>
              <div className={classes.middleLine}></div>
            </Grid>
            <Grid item>
              <button className={classes.ellipse}>150</button>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <p className={classes.cardTitle}>NO. DE REPORTES</p>
                <p className={classes.cardSubtitle2}>hasta 21.01.2020</p>
              </Grid>
            </Grid>
            <Grid item>
              <div className={classes.middleLine}></div>
            </Grid>
            <Grid item>
              <button className={classes.ellipse}>350</button>
            </Grid>
          </Grid>
        </Paper>
        <Grid item xs={12} className={classes.gridList}>
          <ListGroup>
            <ListGroupItem>
              <p className={classes.listTitle}>
                Actualizaciones y últimos movimientos
              </p>
            </ListGroupItem>
            <ListGroupItem>
              <p
                className={classes.listSubtitle}
                style={{ fontWeight: "bold" }}
              >
                11-05-2020 15:30hrs{" "}
                <span style={{ fontWeight: "normal" }}>
                  Se créo un nuevo reporte
                </span>
                .
              </p>
            </ListGroupItem>
            <ListGroupItem>
              <p
                className={classes.listSubtitle}
                style={{ fontWeight: "bold" }}
              >
                11-05-2020 15:30hrs{" "}
                <span style={{ fontWeight: "normal" }}> Se aprobó reporte</span>
                .
              </p>
            </ListGroupItem>
            <ListGroupItem>
              <p
                className={classes.listSubtitle}
                style={{ fontWeight: "bold", wordWrap: "break-word" }}
              >
                {" "}
                11-05-2020 15:30hrs{" "}
                <span style={{ fontWeight: "normal" }}>
                  Se agregó nuevo registro
                </span>
                .
              </p>
            </ListGroupItem>
          </ListGroup>
        </Grid>
      </Grid>
    </div>
  );
};
export default Dashboard;
