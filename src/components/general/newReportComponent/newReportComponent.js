import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import NotificationsIcon from "@material-ui/icons/Notifications";
import FormGroup from "@material-ui/core/FormGroup";
import DateFnsUtils from "@date-io/date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import SearchIcon from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import FilterListIcon from "@material-ui/icons/FilterList";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import Tooltip from "@material-ui/core/Tooltip";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import AssignmentReturnedIcon from "@material-ui/icons/AssignmentReturned";

import "date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Moment from "react-moment";
import "moment-timezone";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input,
  Card,
  CardBody
} from "reactstrap";
import {
  Divider,
  ExpansionPanelActions,
  TextField,
  FormControl,
  Select
} from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import "../../styles.css";

/**
 *
 *  @param {} props
 */

const date = new Date();

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },

  heading: {
    fontSize: "18px",
    fontWeight: theme.typography.fontWeightNormal,
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    lineHeight: "23px",
    color: "#1B1F01"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "justify",
    color: theme.palette.text.secondary,
    boxShadow: "none",
    alignItems: "center"
  },
  paperRegister: {
    padding: theme.spacing(2),
    textAlign: "justify",
    boxShadow: "none",
    alignItems: "center",
    background: "#fafafa"
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: "330px",
    textAlign: "justify",
    border: "1px solid #5d6e02"
  },
  textField: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#615E5E"
  },
  options: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px"
  },
  input: {
    borderBottom: "1px solid #5d6e02",
    background: "#FDFDFD",
    textAlign: "left"
  },
  inputDate: {
    position: "relative",
    top: "-13px",
    borderBottom: "1px solid #424F3E"
  },
  addButton: {
    margin: theme.spacing(2),
    background: "#6b8e23",
    border: "5px solid #6B8E23",
    color: " #FFFFFF",
    borderRadius: "50px",
    width: "45px",
    height: "45px",
    position: "relative"
  },
  cardContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  tableContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  }
}));

const LightTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: "#615E5E",
    color: "#FFFFFF",
    boxShadow: theme.shadows[1],
    fontSize: 13,
    fontFamily: "Raleway",
    fontWeight: "500",
    lineHeight: "15px",
    fontStyle: "normal"
  }
}))(Tooltip);

const NewRegister = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const handleDateChange = date => {
    setSelectedDate(date);
  };

  const [openCreateReport, setOpen] = React.useState(false);
  const toggleNewReport = () => setOpen(!openCreateReport);

  function createData(
    selection,
    locality,
    country,
    state,
    town,
    crop,
    irrigationSystem,
    applicationType,
    sowingSeason
  ) {
    return {
      selection,
      locality,
      country,
      state,
      town,
      crop,
      irrigationSystem,
      applicationType,
      sowingSeason
    };
  }

  const rows = [
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    ),
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    ),
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    ),
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    ),
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    ),
    createData(
      "",
      "Localidad 1",
      "México",
      "Jalisco",
      "Guadalajara",
      "Maíz",
      "Lorem ipsum",
      "Lorem ipsum",
      "Primavera"
    )
  ];

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={7}>
          <p className="principal-title">Nuevo reporte</p>
          <p className="secundary-title">Creación de reportes</p>
          <p className="subtitle">
            Seleccionar parámetros de filtrado para ver la información deseada
          </p>
        </Grid>

        <Grid item xs={5}>
          <Moment format={"dddd DD MMMM YYYY"} className="time">
            {date}
          </Moment>

          <Button onClick={toggle} className={classes.notificationButton}>
            <NotificationsIcon color="#dadada" />5 {buttonLabel}
          </Button>

          <Modal
            isOpen={modal}
            toggle={toggle}
            className={className}
            style={{
              marginTop: 0,
              width: "380px",
              right: "0px",
              position: "fixed"
            }}
          >
            <ModalHeader toggle={toggle}>Panel de notificaciones</ModalHeader>
            <ModalBody style={{ height: "100%" }}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{
                        fontWeight: "bold",
                        marginBottom: "3px",
                        marginTop: "10px"
                      }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "20px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider />
            </ModalBody>
          </Modal>
        </Grid>
        <Grid container spacing={3} direction="row" justify="flex-end">
          <Grid item xs={3}>
            <Paper className={classes.paper}>
              <TextField
                className={classes.input}
                id="newReport-search"
                label=""
                defaultValue="Buscar"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  )
                }}
              />
            </Paper>
          </Grid>
        </Grid>

        <Grid container xs spacing={3}>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-locality-label"
                style={{ marginLeft: "20px" }}
              >
                Localidad
              </InputLabel>
              <Select
                labelId="newReport-locality-label"
                id="newReport-locality"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Localidad uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-country-label"
                style={{ marginLeft: "20px" }}
              >
                País
              </InputLabel>
              <Select
                labelId="newReport-country-label"
                id="newReport-country"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-state-label"
                style={{ marginLeft: "20px" }}
              >
                Estado
              </InputLabel>
              <Select
                labelId="newReport-state-label"
                id="newReport-state"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-town-label"
                style={{ marginLeft: "20px" }}
              >
                Municipio
              </InputLabel>
              <Select
                labelId="newReport-town-label"
                id="newReport-town"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>

          {/* segunda linea */}

          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-crop-label"
                style={{ marginLeft: "20px" }}
              >
                Cultivo
              </InputLabel>
              <Select
                labelId="newReport-crop-label"
                id="newReport-crop"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}> uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-irrigationSystem-label"
                style={{ marginLeft: "20px" }}
              >
                Sistema de riego
              </InputLabel>
              <Select
                labelId="newReport-irrigationSystem-label"
                id="newReport-irrigationSystem"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-applicationType-label"
                style={{ marginLeft: "20px" }}
              >
                Tipo de aplicación
              </InputLabel>
              <Select
                labelId="newReport-applicationType-label"
                id="newReport-applicationType"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-sowingSeason-label"
                style={{ marginLeft: "20px" }}
              >
                Época de siembra
              </InputLabel>
              <Select
                labelId="newReport-sowingSeason-label"
                id="newReport-sowingSeason"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>

          {/* tercer linea */}
          <Grid item xs={3}>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel
                id="newReport-ingredients-label"
                style={{ marginLeft: "20px" }}
              >
                Ingrediente
              </InputLabel>
              <Select
                labelId="newReport-ingredients-label"
                id="newReport-ingredients"
                value=""
                onChange=""
              >
                <MenuItem value=" ">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Uno </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid
            item
            xs
            style={{ textAlign: "left", margin: "0px 0px 0px 15px" }}
          >
            <p className="subtitle-date">Fechas de plantación</p>
            <Grid container direction="row" spacing={3}>
              <Grid item xs={4}>
                <Paper className={classes.paperRegister}>
                  <FormGroup>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="newReport-plantingDate"
                        label="Desde esta fecha"
                        value={selectedDate}
                        onChange={handleDateChange}
                        className={classes.inputDate}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormGroup>
                </Paper>
              </Grid>
              <p style={{ margin: "50px 10px 0px 10px" }}> - </p>
              <Grid item xs={4}>
                <Paper className={classes.paperRegister}>
                  <FormGroup>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        id="newReport-plantingDate"
                        label="Hasta esta fecha"
                        value={selectedDate}
                        onChange={handleDateChange}
                        className={classes.inputDate}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormGroup>
                </Paper>
              </Grid>
              <Grid item xs>
                <FormGroup>
                  <Button style={{ background: "#6b8e23" }}>
                    <FilterListIcon /> FILTRAR INFORMACIÓN
                  </Button>
                </FormGroup>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* tabla */}
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.cardContainer}>
              <CardBody>
                <Grid item xs={12}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell className={classes.tableHead}>
                            Seleccionar
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Localidad <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            País <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Estado <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Municipio <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Cultivo <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Sistema de riego <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Tipo de aplicación <ImportExportIcon />
                          </TableCell>
                          <TableCell className={classes.tableHead}>
                            Época de siembra <ImportExportIcon />
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {rows.map(row => (
                          <TableRow key={row.locality}>
                            <TableCell>
                              <Checkbox
                                inputProps={{
                                  "aria-label": "uncontrolled-checkbox"
                                }}
                              />
                            </TableCell>
                            <TableCell
                              component="th"
                              scope="row"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.locality}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.country}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              letterSpacing="-0.15em"
                              className={classes.options}
                            >
                              {row.state}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.town}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.crop}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.irrigationSystem}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.applicationType}
                            </TableCell>
                            <TableCell
                              align="right"
                              color=" #333333"
                              className={classes.options}
                            >
                              {row.sowingSeason}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </CardBody>
            </Card>
            <div>
              <Button
                type="button"
                onClick={toggleNewReport}
                className={classes.addButton}
                style={{ background: "#6b8e23" }}
              >
                <LightTooltip title="Crear reporte" placement="top">
                  <AssignmentTurnedInIcon style={{ marginLeft: "-5px" }} />
                </LightTooltip>
              </Button>

              <Modal isOpen={openCreateReport} toggle={toggleNewReport}>
                <ModalBody>
                  <CheckCircleOutlineIcon
                    style={{
                      height: "8em",
                      width: "8em",
                      marginLeft: "30%",
                      marginTop: "50px"
                    }}
                  />
                  <p className="modalNewReport">Reporte creado con éxito</p>
                  <Button
                    style={{
                      background: "#6B8E23",
                      border: "5px solid #6B8E23",
                      marginTop: "20px",
                      marginLeft: "30%"
                    }}
                  >
                    <AssignmentReturnedIcon /> Descargar
                  </Button>
                </ModalBody>
              </Modal>
            </div>
          </Grid>
        </Grid>
      </Grid>{" "}
      {/* contenedor */}
    </div>
  );
};
export default NewRegister;
