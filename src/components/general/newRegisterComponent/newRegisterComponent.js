import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/core/styles";
import { green, grey } from "@material-ui/core/colors";
import AddIcon from "@material-ui/icons/Add";
import DateFnsUtils from "@date-io/date-fns";
import PublishIcon from "@material-ui/icons/Publish";
import MenuItem from "@material-ui/core/MenuItem";
import AddBoxIcon from "@material-ui/icons/AddBox";
import SaveIcon from "@material-ui/icons/Save";
import "date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Moment from "react-moment";
import "moment-timezone";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input,
  Card,
  CardBody
} from "reactstrap";
import { Divider, ExpansionPanelActions, TextField } from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import "../../styles.css";

/**
 *
 *  @param {} props
 */

const date = new Date();

const ColorSwitch = withStyles({
  switchBase: {
    color: grey[50],
    "&$checked": {
      color: green[400]
    },
    "&$checked + $track": {
      backgroundColor: green[500]
    }
  },
  checked: {},
  track: {}
})(Switch);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },
  heading: {
    fontSize: "18px",
    fontWeight: theme.typography.fontWeightNormal,
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    lineHeight: "23px",
    color: "#1B1F01"
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "justify",
    color: theme.palette.text.secondary,
    boxShadow: "none",
    alignItems: "center"
  },
  paperTreatment: {
    padding: theme.spacing(2),
    textAlign: "justify",
    boxShadow: "none",
    alignItems: "center",
    background: "lightgrey"
  },

  paperAplication: {
    padding: theme.spacing(2),
    textAlign: "justify",
    boxShadow: "none",
    alignItems: "center",
    background: "#f8f7f7"
  },
  textField: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#615E5E"
  },
  options: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px"
  },
  input: {
    borderBottom: "1px solid #424F3E",
    // background: "#FDFDFD",
    textAlign: "left"
  },
  inputDate: {
    position: "relative",
    top: "-13px",
    borderBottom: "1px solid #424F3E"
  },
  addButton: {
    margin: theme.spacing(2),
    background: "#6b8e23",
    border: "5px solid #6B8E23",
    color: " #FFFFFF",
    borderRadius: "50px",
    width: "35px",
    height: "35px",
    left: "-30px",
    position: "relative"
  },
  cardContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  tableContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  inputNotes: {
    border: "none",
    borderBottom: "1px solid #424F3E",
    textAlign: "left",
    background: "none"
  }
}));

const currencies = [
  {
    value: "USD",
    label: "$"
  },
  {
    value: "EUR",
    label: "€"
  },
  {
    value: "BTC",
    label: "฿"
  },
  {
    value: "JPY",
    label: "¥"
  }
];

const NewRegister = props => {
  const classes = useStyles();
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true
  });
  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const [currency, setCurrency] = React.useState("USD");

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const handleDateChange = date => {
    setSelectedDate(date);
  };

  const handleChangeConcurrency = event => {
    setCurrency(event.target.value);
  };
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <p className="principal-title">Nuevo Registro</p>
          <p className="secundary-title">Generador de registros</p>
        </Grid>
        <Grid item xs={6}>
          <Moment format={"dddd DD MMMM YYYY"} className="time">
            {date}
          </Moment>

          <Button onClick={toggle} className={classes.notificationButton}>
            <NotificationsIcon color="#dadada" />5 {buttonLabel}
          </Button>

          <Modal
            isOpen={modal}
            toggle={toggle}
            className={className}
            style={{
              marginTop: 0,
              width: "380px",
              right: "0px",
              position: "fixed"
            }}
          >
            <ModalHeader toggle={toggle}>Panel de notificaciones</ModalHeader>
            <ModalBody style={{ height: "100%" }}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "10px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{
                        fontWeight: "bold",
                        marginBottom: "3px",
                        marginTop: "10px"
                      }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider style={{ marginBottom: "10px" }} />
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <PublicIcon
                    style={{ textAlign: "center", marginTop: "20px" }}
                  />
                </Grid>
                <Grid
                  item
                  xs={10}
                  style={{ textAlign: "left", marginBottom: "10px" }}
                >
                  <div>
                    <Moment
                      format={"DD MM YY kk:mm [hrs]"}
                      className="notification-style"
                      style={{ fontWeight: "bold", marginBottom: "3px" }}
                    >
                      {date}
                    </Moment>
                  </div>
                  <div
                    className="notification-style"
                    style={{ fontWeight: "normal", marginBottom: "3px" }}
                  >
                    Nueva solicitud de aprobación de idioma
                  </div>
                  <div className="notification-style-approval">APROBAR</div>
                </Grid>
              </Grid>
              <Divider />
            </ModalBody>
          </Modal>
          <Grid item xs style={{ marginTop: "30px" }}>
            <FormControlLabel
              control={
                <ColorSwitch
                  checked={state.checkedA}
                  onChange={handleChange("checkedA")}
                  value="checkedA"
                />
              }
              label="Definir como trabajo oficial"
              className="switch-label"
            />
          </Grid>
        </Grid>
      </Grid>
      <Card className={classes.cardContainer}>
        <CardBody>
          <Grid item xs={12}>
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  Datos de localidad
                </Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="locationData-locality"
                        select
                        label="Localidad"
                        value="Seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value="Seleccionar"
                        >
                          Seleccionar
                        </MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="locationData-country"
                        select
                        label="País"
                        value="Seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value="Seleccionar"
                        >
                          Seleccionar
                        </MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="locationData-state"
                        select
                        label="Estado"
                        value="Seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value="Seleccionar"
                        >
                          Seleccionar
                        </MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="locationData-town"
                        select
                        label="Municipio"
                        value="Seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value="Seleccionar"
                        >
                          Seleccionar
                        </MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            {/* 2do  datos de productor*/}
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  Datos de productor
                </Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid item xs={6}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="producerData-manufacturerName"
                        select
                        label="Nombre del productor"
                        value="Seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value="Seleccionar"
                        >
                          Seleccionar
                        </MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            {/* 3er socio productor */}
            <ExpansionPanel>
              <ExpansionPanel>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className={classes.heading}>
                    Datos de socio productor
                  </Typography>
                </ExpansionPanelSummary>

                <ExpansionPanelDetails>
                  <Grid item xs={6}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="producerDetails-associateName"
                          select
                          label="Nombre de socio"
                          value="Seleccionar"
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value="Seleccionar"
                          >
                            Seleccionar
                          </MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}>
                    <Paper className={classes.paper}>
                      <Button variant="contained" className={classes.addButton}>
                        <AddIcon
                          style={{ marginLeft: "-12px", marginTop: "-9px" }}
                        />
                      </Button>
                    </Paper>
                  </Grid>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </ExpansionPanel>
            {/* 4to  datos de cultivo*/}
            <ExpansionPanel>
              <ExpansionPanel>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className={classes.heading}>
                    Datos de cultivo
                  </Typography>
                </ExpansionPanelSummary>

                <ExpansionPanelDetails>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-crop"
                          select
                          label="Cultivo"
                          value="Seleccionar"
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value="Seleccionar"
                          >
                            Seleccionar
                          </MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}>
                    <Paper className={classes.paper}>
                      <Button variant="contained" className={classes.addButton}>
                        <AddIcon
                          style={{ marginLeft: "-12px", marginTop: "-9px" }}
                        />
                      </Button>
                    </Paper>
                  </Grid>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-cropVariety"
                          select
                          label="Variedad de cultivo"
                          value="Seleccionar"
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value="Seleccionar"
                          >
                            Seleccionar
                          </MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}></Grid>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="cropData-plantingDate"
                            label="Fecha de plantación / siembra"
                            value={selectedDate}
                            onChange={handleDateChange}
                            className={classes.inputDate}
                            KeyboardButtonProps={{
                              "aria-label": "change date"
                            }}
                          />
                        </MuiPickersUtilsProvider>
                      </FormGroup>
                    </Paper>
                  </Grid>
                </ExpansionPanelDetails>

                {/* segunda linea */}
                <ExpansionPanelDetails>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-plantingSeason"
                          select
                          label="Época de siembra"
                          value="Seleccionar"
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value="Seleccionar"
                          >
                            Seleccionar
                          </MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}>
                    <Paper className={classes.paper}>
                      <Button variant="contained" className={classes.addButton}>
                        <AddIcon
                          style={{ marginLeft: "-12px", marginTop: "-9px" }}
                        />
                      </Button>
                    </Paper>
                  </Grid>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            format="MM/dd/yyyy"
                            margin="normal"
                            id="cropData-harvestDate"
                            label="Fecha de cosecha"
                            value={selectedDate}
                            onChange={handleDateChange}
                            className={classes.inputDate}
                            KeyboardButtonProps={{
                              "aria-label": "change date"
                            }}
                          />
                        </MuiPickersUtilsProvider>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid itme xs={1}></Grid>
                  <Grid item xs={2}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-irrigationSystem"
                          select
                          label="Sistema de riego"
                          value="Seleccionar"
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value="Seleccionar"
                          >
                            Seleccionar
                          </MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}>
                    <Paper className={classes.paper}>
                      <Button variant="contained" className={classes.addButton}>
                        <AddIcon
                          style={{ marginLeft: "-12px", marginTop: "-9px" }}
                        />
                      </Button>
                    </Paper>
                  </Grid>
                </ExpansionPanelDetails>
                {/* 3er fila */}
                <ExpansionPanelDetails>
                  <Grid item xs={3}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-lotSize"
                          label="Tamaño de lote"
                          value="  "
                          onChange=""
                        ></TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                  <Grid item xs={1}>
                    <Paper className={classes.paper}>
                      <FormGroup>
                        <TextField
                          className={classes.input}
                          id="cropData-lotSizeUnit"
                          select
                          label="Seleccionar"
                          value=" "
                          onChange=""
                        >
                          <MenuItem
                            className={classes.options}
                            key=""
                            value=""
                          ></MenuItem>
                        </TextField>
                      </FormGroup>
                    </Paper>
                  </Grid>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </ExpansionPanel>
            {/* 5to  protocolos*/}
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>Protocolos</Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <Button
                      id="protocols-uploadFileButton"
                      variant="contained"
                      style={{ background: "#6b8e23" }}
                    >
                      <PublishIcon /> CARGAR ARCHIVO
                    </Button>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <img src="../../../images/pdfFile.png" alt="" />

                    <Button
                      id="protocols-seeFileButton"
                      style={{
                        marginLeft: "12px",
                        top: "-9px",
                        position: "relative",
                        background: "#6b8e23"
                      }}
                    >
                      Ver
                    </Button>
                    <DeleteForeverIcon
                      id="protocols-deleteFileButton"
                      style={{
                        top: "27px",
                        left: "-35.75px",
                        position: "relative"
                      }}
                    />
                    <p>Protocolo 1</p>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <img src="../../../images/pdfFile.png" alt="" />
                    <Button
                      style={{
                        marginLeft: "12px",
                        top: "-9px",
                        position: "relative"
                      }}
                    >
                      Ver
                    </Button>
                    <DeleteForeverIcon
                      style={{
                        top: "27px",
                        left: "-35.75px",
                        position: "relative"
                      }}
                    />
                    <p>Protocolo 2</p>
                  </Paper>
                </Grid>

                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <img src="../../../images/pdfFile.png" alt="" />
                    <Button
                      style={{
                        marginLeft: "12px",
                        top: "-9px",
                        position: "relative"
                      }}
                    >
                      Ver
                    </Button>
                    <DeleteForeverIcon
                      style={{
                        top: "27px",
                        left: "-35.75px",
                        position: "relative"
                      }}
                    />
                    <p>Protocolo 3</p>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <img src="../../../images/pdfFile.png" alt="" />
                    <Button
                      style={{
                        marginLeft: "12px",
                        top: "-9px",
                        position: "relative"
                      }}
                    >
                      Ver
                    </Button>
                    <DeleteForeverIcon
                      style={{
                        top: "27px",
                        left: "-35.75px",
                        position: "relative"
                      }}
                    />
                    <p>Protocolo 4</p>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            {/* 6to tratamiento testigo */}
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  Tratamiento testigo
                </Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <Typography className={classes.heading}>
                      Aplicación No.1
                    </Typography>

                    <Divider />
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-applicationType"
                        select
                        label="Tipo de aplicación"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=""
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-controlType"
                        select
                        label="Tipo de testigo"
                        value="seleccionar"
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=""
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
                <Grid item xs={4}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="controlTreatment-applicationDate"
                          label="Fecha de aplicación"
                          value={selectedDate}
                          onChange={handleDateChange}
                          className={classes.inputDate}
                          KeyboardButtonProps={{
                            "aria-label": "change date"
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </FormGroup>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
              {/* segunda linea */}
              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-dose"
                        label="Dosis"
                        value="  "
                        onChange=""
                      ></TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-doseUnit"
                        select
                        label="Seleccionar"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=" "
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-loteSize"
                        select
                        label="Tamaño de lote"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=""
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-loteSizeUnit"
                        select
                        label="Seleccionar"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=" "
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={2}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        select
                        label="Costo de tratamiento"
                        value={currency}
                        onChange={handleChangeConcurrency}
                        name="treatmentCostCurrency"
                        id="controlTreatment-currency"
                        className={classes.input}
                      >
                        Moneda
                        {currencies.map(option => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={2}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-quantity"
                        select
                        label="Cantidad"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=""
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
              {/* 3ra fila */}
              <ExpansionPanelDetails>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-performance"
                        label="Rendimiento"
                        value="  "
                        onChange=""
                      ></TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="controlTreatment-performanceUnit"
                        select
                        label="Seleccionar"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=" "
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
              <ExpansionPanelActions>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                >
                  <Button
                    style={{ background: "#6b8e23", marginBottom: "20px" }}
                  >
                    <AddBoxIcon /> AGREGAR APLICACIÓN
                  </Button>
                </Grid>
              </ExpansionPanelActions>
            </ExpansionPanel>
            {/* 7mo tratamientos */}

            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  Tratamientos
                </Typography>
              </ExpansionPanelSummary>

              {/* <Card className={classes.cardContainer}> */}
              <CardBody
                style={{
                  background: " lightgrey",
                  marginLeft: "20px",
                  marginRight: "20px"
                }}
              >
                <ExpansionPanelDetails>
                  <Grid item xs={12}>
                    <Paper className={classes.paperTreatment}>
                      <Typography className={classes.heading}>
                        Añadir Tratamiento
                      </Typography>
                    </Paper>
                    <Divider />

                    <ExpansionPanelDetails>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-applicationType"
                              select
                              label="Tipo de aplicación"
                              value="  "
                              onChange=""
                            >
                              <MenuItem
                                className={classes.options}
                                key=""
                                value=""
                              ></MenuItem>
                            </TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={1}>
                        <Paper className={classes.paperTreatment}>
                          <Button
                            variant="contained"
                            className={classes.addButton}
                          >
                            <AddIcon
                              style={{ marginLeft: "-12px", marginTop: "-9px" }}
                            />
                          </Button>
                        </Paper>
                      </Grid>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                              <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="treatment-applicationDate"
                                label="Fecha de aplicación"
                                value={selectedDate}
                                onChange={handleDateChange}
                                className={classes.inputDate}
                                KeyboardButtonProps={{
                                  "aria-label": "change date"
                                }}
                              />
                            </MuiPickersUtilsProvider>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-dose"
                              label="Dosis"
                              value="  "
                              onChange=""
                            ></TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={1}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-doseUnit"
                              select
                              label="Seleccionar"
                              value="  "
                              onChange=""
                            >
                              <MenuItem
                                className={classes.options}
                                key=""
                                value=" "
                              ></MenuItem>
                            </TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                    </ExpansionPanelDetails>
                    {/* 2da linea */}
                    <ExpansionPanelDetails>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-lotSize"
                              label="Tamaño de lote"
                              value="  "
                              onChange=""
                            ></TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={1}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-lotSizeUnit"
                              select
                              label="Seleccionar"
                              value=" "
                              onChange=""
                            >
                              <MenuItem
                                className={classes.options}
                                key=""
                                value=" "
                              ></MenuItem>
                            </TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              select
                              label="Costo de tratamiento"
                              value={currency}
                              onChange={handleChangeConcurrency}
                              name="treatmentCurrency"
                              id="treatment-currency"
                              className={classes.input}
                            >
                              Moneda
                              {currencies.map(option => (
                                <MenuItem
                                  key={option.value}
                                  value={option.value}
                                >
                                  {option.label}
                                </MenuItem>
                              ))}
                            </TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={3}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-performanceUnit"
                              label="Rendimiento"
                              value="  "
                              onChange=""
                            ></TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                      <Grid item xs={1}>
                        <Paper className={classes.paperTreatment}>
                          <FormGroup>
                            <TextField
                              className={classes.input}
                              id="treatment-performance"
                              select
                              label="Seleccionar"
                              value=" "
                              onChange=""
                            >
                              <MenuItem
                                className={classes.options}
                                key=""
                                value=""
                              ></MenuItem>
                            </TextField>
                          </FormGroup>
                        </Paper>
                      </Grid>
                    </ExpansionPanelDetails>

                    {/* card anidada */}
                    <Card style={{ background: "#F8F7F7" }}>
                      <CardBody>
                        <ExpansionPanelSummary>
                          <Grid container spacing={2}>
                            <Grid item xs={12}>
                              <Paper className={classes.paperAplication}>
                                <Typography className={classes.heading}>
                                  Aplicación No.1
                                </Typography>

                                <Divider />
                              </Paper>
                            </Grid>
                            <Grid item xs={4}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentAplication-applicationType"
                                    select
                                    label="Tipo de aplicación"
                                    value="  "
                                    onChange=""
                                  >
                                    <MenuItem
                                      className={classes.options}
                                      key=""
                                      value=""
                                    ></MenuItem>
                                  </TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>

                            <Grid item xs={4}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                      disableToolbar
                                      variant="inline"
                                      format="MM/dd/yyyy"
                                      margin="normal"
                                      id="treatmentApplication-applicationDate"
                                      label="Fecha de aplicación"
                                      value={selectedDate}
                                      onChange={handleDateChange}
                                      className={classes.inputDate}
                                      KeyboardButtonProps={{
                                        "aria-label": "change date"
                                      }}
                                    />
                                  </MuiPickersUtilsProvider>
                                </FormGroup>
                              </Paper>
                            </Grid>
                            <Grid item xs={3}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentApplication-dose"
                                    label="Dosis"
                                    value="  "
                                    onChange=""
                                  ></TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>
                            <Grid item xs={1}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentApplication-doseUnit"
                                    select
                                    label="Seleccionar"
                                    value="  "
                                    onChange=""
                                  >
                                    <MenuItem
                                      className={classes.options}
                                      key=""
                                      value=" "
                                    ></MenuItem>
                                  </TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>
                          </Grid>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                          <Grid item xs={2}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  select
                                  label="Costo de tratamiento"
                                  value={currency}
                                  onChange={handleChangeConcurrency}
                                  name="treatmentCurrency"
                                  id="treatmentAplication-currency"
                                  className={classes.input}
                                >
                                  Moneda
                                  {currencies.map(option => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                          <Grid item xs={2}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  className={classes.input}
                                  id="treatmentApplication-performanceUnit"
                                  label="Cantidad"
                                  value="  "
                                  onChange=""
                                ></TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                          <Grid item xs={4}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  className={classes.input}
                                  id="treatmentApplication-ingredients"
                                  label="Ingredientes"
                                  select
                                  value="  "
                                  onChange=""
                                >
                                  <MenuItem
                                    className={classes.options}
                                    key=""
                                    value=" "
                                  ></MenuItem>
                                </TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                        </ExpansionPanelDetails>

                        {/* segunda aplicacion */}
                        <ExpansionPanelSummary>
                          <Grid container spacing={2}>
                            <Grid item xs={12}>
                              <Paper className={classes.paperAplication}>
                                <Typography className={classes.heading}>
                                  Aplicación No.2
                                </Typography>

                                <Divider />
                              </Paper>
                            </Grid>
                            <Grid item xs={4}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentAplicationTwo-applicationType"
                                    select
                                    label="Tipo de aplicación"
                                    value="  "
                                    onChange=""
                                  >
                                    <MenuItem
                                      className={classes.options}
                                      key=""
                                      value=""
                                    ></MenuItem>
                                  </TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>

                            <Grid item xs={4}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                      disableToolbar
                                      variant="inline"
                                      format="MM/dd/yyyy"
                                      margin="normal"
                                      id="treatmentApplicationTwo-applicationDate"
                                      label="Fecha de aplicación"
                                      value={selectedDate}
                                      onChange={handleDateChange}
                                      className={classes.inputDate}
                                      KeyboardButtonProps={{
                                        "aria-label": "change date"
                                      }}
                                    />
                                  </MuiPickersUtilsProvider>
                                </FormGroup>
                              </Paper>
                            </Grid>
                            <Grid item xs={3}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentApplicationTwo-dose"
                                    label="Dosis"
                                    value="  "
                                    onChange=""
                                  ></TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>
                            <Grid item xs={1}>
                              <Paper className={classes.paperAplication}>
                                <FormGroup>
                                  <TextField
                                    className={classes.input}
                                    id="treatmentApplicationTwo-doseUnit"
                                    select
                                    label="Seleccionar"
                                    value="  "
                                    onChange=""
                                  >
                                    <MenuItem
                                      className={classes.options}
                                      key=""
                                      value=""
                                    ></MenuItem>
                                  </TextField>
                                </FormGroup>
                              </Paper>
                            </Grid>
                          </Grid>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                          <Grid item xs={2}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  select
                                  label="Costo de tratamiento"
                                  value={currency}
                                  onChange={handleChangeConcurrency}
                                  name="treatmentCurrency"
                                  id="treatmentAplicationTwo-currency"
                                  className={classes.input}
                                >
                                  {currencies.map(option => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                          <Grid item xs={2}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  className={classes.input}
                                  id="treatmentApplicationTwo-performanceUnit"
                                  label="Cantidad"
                                  value="  "
                                  onChange=""
                                ></TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                          <Grid item xs={4}>
                            <Paper className={classes.paperAplication}>
                              <FormGroup>
                                <TextField
                                  className={classes.input}
                                  id="treatmentApplicationTwo-ingredients"
                                  label="Ingredientes"
                                  select
                                  value="  "
                                  onChange=""
                                >
                                  <MenuItem
                                    className={classes.options}
                                    key=""
                                    value=" "
                                  ></MenuItem>
                                </TextField>
                              </FormGroup>
                            </Paper>
                          </Grid>
                        </ExpansionPanelDetails>
                        {/* boton */}
                        <ExpansionPanelDetails>
                          <Grid
                            container
                            direction="column"
                            justify="center"
                            alignItems="center"
                          >
                            <Button
                              style={{
                                background: "#6b8e23",
                                marginBottom: "20px"
                              }}
                            >
                              <AddBoxIcon /> NUEVA APLICACIÓN
                            </Button>
                          </Grid>
                        </ExpansionPanelDetails>
                      </CardBody>
                    </Card>
                  </Grid>
                </ExpansionPanelDetails>
                <ExpansionPanelDetails>
                  <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                  >
                    <Button
                      style={{
                        background: "#6b8e23",
                        marginBottom: "20px"
                      }}
                    >
                      <SaveIcon /> GUARDAR TRATAMIENTO
                    </Button>
                  </Grid>
                </ExpansionPanelDetails>
              </CardBody>
            </ExpansionPanel>

            {/* 8vo parametros evaluacion */}
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>
                  Parámetros de evaluación
                </Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid item xs={4}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="evaluationParameters-parameterType"
                        select
                        label="Tipo de parámetro"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=""
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
                <Grid item xs={4}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="evaluationParameters-evaluationDate"
                          label="Fecha de evaluación"
                          value={selectedDate}
                          onChange={handleDateChange}
                          className={classes.inputDate}
                          KeyboardButtonProps={{
                            "aria-label": "change date"
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="evaluationParameters-value"
                        label="Valor"
                        value="  "
                        onChange=""
                      ></TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <FormGroup>
                      <TextField
                        className={classes.input}
                        id="evaluationParameters-valueUnit"
                        select
                        label="Seleccionar"
                        value="  "
                        onChange=""
                      >
                        <MenuItem
                          className={classes.options}
                          key=""
                          value=" "
                        ></MenuItem>
                      </TextField>
                    </FormGroup>
                  </Paper>
                </Grid>
                <Grid item xs={1}>
                  <Paper className={classes.paper}>
                    <Button variant="contained" className={classes.addButton}>
                      <AddIcon
                        style={{ marginLeft: "-12px", marginTop: "-9px" }}
                      />
                    </Button>
                  </Paper>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Grid>
          <Grid item xs={12} style={{ marginTop: "30px" }}>
            <Paper className={classes.paper}>
              <FormGroup>
                <TextField
                  className={classes.input}
                  id="notes"
                  label="Notas"
                  defaultValue="Escríbe aquí"
                  onChange=""
                ></TextField>
              </FormGroup>
            </Paper>
          </Grid>
          <Grid
            container
            style={{ marginTop: "30px" }}
            direction="row"
            justify="center"
            alignItems="space-between"
            spacing={3}
          >
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                style={{ background: "#6b8e23" }}
              >
                GUARDAR REGISTRO
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                style={{ background: "#6b8e23" }}
              >
                CANCELAR REGISTRO
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="secondary"
                style={{ background: "#6b8e23" }}
              >
                AUTORIZAR REGISTRO
              </Button>
            </Grid>
          </Grid>
        </CardBody>
      </Card>
    </div>
  );
};
export default NewRegister;
