import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 110;

const useStyles = makeStyles(theme => ({
  buttonMain: {
    color: "white",
    background: "green",
    width: "100%",
    padding: "10px"
  },
  subtitleVerdeLight: {
    color: "#6B8E23",
    fontFamily: "RalewaySemiBold",
    fontSize: "28px"
  },
  subtitleGreenDark: {
    color: "#214915",
    fontFamily: "RalewaySemiBold",
    fontSize: "30px"
  },
  backgroundBody: {
    backgroundImage: "url('./../../images/group_57.png')",
    backgroundRepeat: "no-repeat",
    height: "56em",
    backgroundSize: "contain"
  },
  form: {
    display: "flex",
    margin: "auto 50px auto auto"
  },
  footer: {
    background:
      "linear-gradient(0deg, rgba(27,31,1,0.47942927170868344) 0%, rgba(255,255,255,1) 100%)",
    textAlign: "center",
    padding: "7px"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  footerParagraph: {
    textAlign: "center"
  },
  navbarLogo: {
    position: "relative",
    top: "27px"
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(6)
  }
}));

export default useStyles;
