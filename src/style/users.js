import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  notificationButton: {
    background: "#93A989",
    border: "5px solid #93A989",
    width: "80px",
    height: "50px",
    borderRadius: "5px",
    textAlign: "center",
    color: "#ffff",
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontSize: "24px",
    lineHeight: "30px"
  },
  heading: {
    fontSize: "18px",
    fontWeight: theme.typography.fontWeightNormal,
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    lineHeight: "23px",
    color: "#1B1F01"
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
    boxShadow: "none",
    alignItems: "center"
  },
  textField: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "13px",
    lineHeight: "16px",
    color: "#615E5E"
  },
  options: {
    fontFamily: "Source Sans Pro",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "20px",
    textAlign: "justify"
  },
  input: {
    border: "none",
    borderBottom: "1px solid #424F3E",
    width: "300px",
    background: "#FDFDFD"
  },
  buttonCollapse: {
    margin: theme.spacing(1),
    background: "#6b8e23",
    border: "5px solid #6B8E23",
    color: " #FFFFFF"
  },
  cardContainer: {
    marginTop: "50px",
    background: "#F8F7F7",
    padding: "10px 5px"
  },
  tableContainer: {
    marginTop: "50px",
    background: "#F8F7F7"
  },
  tableHead: {
    fontFamily: "Raleway",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "18px",
    lineHeight: "21px",
    alignItems: "center",
    letterSpacing: "-0.015em",
    color: "#1C2000",
    textAlign: "justify"
  },
  table: {
    minWidth: 650
  }
}));

export default useStyles;
